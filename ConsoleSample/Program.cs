﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace ConsoleSample
{
    class Program
    {
        private const string IdSever = "https://localhost:6001";
        private const string ClientID = "m2m.client";
        private const string ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A";

        private const string IdService = "https://localhost:5001/WeatherForecast";

        static async Task Main(string[] args)
        {
            var httpClientHandler = new HttpClientHandler();
            var client = new HttpClient(httpClientHandler);
            var disco = await client.GetDiscoveryDocumentAsync(IdSever);

            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            var tokenClient = new TokenClient(client, new TokenClientOptions()
            {
                Address = disco.TokenEndpoint,
                ClientId = ClientID,
                ClientSecret = ClientSecret
            });

            var tokenResponse = await tokenClient.RequestClientCredentialsTokenAsync("scope1");

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine($"Token:  \n\n {tokenResponse.AccessToken}");

            //       client = new HttpClient(httpClientHandler);

            client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync(IdService);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
                return;
            }

            var content = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"service response: \n\n {content}");

            Console.ReadLine();
        }
    }
}
